package ru.tsc.karbainova.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    void clear();

    void addAll(Collection<E> collection);

    E findById(@Nullable String id);

    void removeById(@Nullable String id);

    void remove(E entity);

    E add(final E entity);

    //E findById(final String id);

    boolean exists(final String id);
}
