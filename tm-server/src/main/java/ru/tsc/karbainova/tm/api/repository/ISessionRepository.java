package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {

}
