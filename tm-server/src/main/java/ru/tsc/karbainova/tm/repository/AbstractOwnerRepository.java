package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.IOwnerRepository;
import ru.tsc.karbainova.tm.model.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    public AbstractOwnerRepository(@NonNull Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull E entity) {
        @NonNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ? and user_id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void clear(@NonNull String userId) {
        @NonNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public List<E> findAll(@NonNull final String userId) {
        @NonNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        @NonNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public List<E> findAll(@NonNull final String userId, Comparator<E> comparator) {
        @NonNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        List<E> result = new ArrayList<>();
        if (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    public void add(@NonNull final String userId, E entity) {
        if (entity == null) return;
        entity.setUserId(userId);
        add(entity);
    }

    @Override
    @SneakyThrows
    public E findById(@NonNull final String userId, @NonNull final String id) {
        @NonNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NonNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NonNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeById(@NonNull final String userId, @Nullable final String id) {
        @NonNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ? and user_id = ?";
        @NonNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void addAll(@NonNull final String userId, final Collection<E> collection) {
        if (collection == null) return;
        for (E i : collection) {
            i.setUserId(userId);
            add(i);
        }
    }
}
