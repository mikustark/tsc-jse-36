package ru.tsc.karbainova.tm.api.repository;

import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {
    void add(String userId, Task task);

    void remove(String userId, Task task);

    boolean existsById(String userId, String id);

    void addAll(Collection<Task> tasks);

//    void clear();

    List<Task> findAll();

    List<Task> findAll(String userId);

    List<Task> findAll(String userId, Comparator<Task> comparator);

//    void clear(String userId);

    Task findById(String userId, String id);

    Task update(Task entity);

    Task findByIndex(String userId, int index);

    Task findByName(String userId, String name);

    void removeById(String userId, String id);

    void removeByName(String userId, String name);

    void removeByIndex(String userId, int index);

    Task taskUnbindById(String userId, String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

    Task bindTaskToProjectById(String userId, String projectId, String taskId);

    List<Task> findAllTaskByProjectId(String userId, String projectId);

}
