package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.ISaltSettings;
import ru.tsc.karbainova.tm.api.repository.IUserRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.IUserService;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.empty.*;
import ru.tsc.karbainova.tm.model.User;
import ru.tsc.karbainova.tm.repository.UserRepository;
import ru.tsc.karbainova.tm.util.HashUtil;

import java.sql.Connection;

public class UserService extends AbstractService<User> implements IUserService {

    public UserService(IConnectionService connectionService) {
        super(connectionService);
    }

    public IUserRepository getRepository(@NonNull Connection connection) {
        return new UserRepository(connection);
    }

    public boolean isLoginExists(@NonNull final String login) {
        if (login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    @SneakyThrows
    public User findByLogin(@NonNull final String login) {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IUserRepository userRepository = getRepository(connection);
            return userRepository.findByLogin(login);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public User findByEmail(@NonNull final String email) {
        if (email.isEmpty()) throw new EmptyEmailException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IUserRepository userRepository = getRepository(connection);
            return userRepository.findByEmail(email);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public User create(@NonNull final String login, @NonNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IUserRepository userRepository = getRepository(connection);
            userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public User create(@NonNull final String login, @NonNull final String password, @NonNull final String email) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new EmptyLoginException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IUserRepository userRepository = getRepository(connection);
            @Nullable final User user = create(login, password);
            if (user == null) return null;
            user.setEmail(email);
            userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public User setPassword(@NonNull final String userId, @NonNull final String password) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        IPropertyService propertyService = new PropertyService();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IUserRepository userRepository = getRepository(connection);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) return null;
            final String hash = HashUtil.salt(propertyService, password);
            user.setPasswordHash(hash);
            connection.commit();
            return user;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }


    @Override
    @SneakyThrows
    public User updateUser(
            @NonNull final String userId,
            @NonNull final String firstName,
            @NonNull final String lastName,
            @Nullable final String middleName) {
        if (userId.isEmpty()) throw new EmptyIdException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IUserRepository userRepository = getRepository(connection);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) return null;
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            userRepository.update(user);
            connection.commit();
            return user;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }
}
