package ru.tsc.karbainova.tm.command.data;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import javax.xml.bind.*;
import java.io.FileOutputStream;

public class DataJsonXmlSaveJaxBCommand extends AbstractDataCommand {

    @Override
    public String name() {
        return "json-jaxb-save";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Json jax-b save";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.setProperty(SYSTEM_JSON_PROPERTY_NAME, SYSTEM_JSON_PROPERTY_VALUE);
        @NonNull final Domain domain = getDomain();
        @NonNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NonNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(JAXB_JSON_PROPERTY_NAME, JAXB_JSON_PROPERTY_VALUE);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NonNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JAXB_JSON);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();

    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
