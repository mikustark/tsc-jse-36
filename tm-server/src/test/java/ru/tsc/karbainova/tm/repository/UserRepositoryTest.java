package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.karbainova.tm.model.User;
import ru.tsc.karbainova.tm.service.ConnectionService;
import ru.tsc.karbainova.tm.service.PropertyService;

import java.util.List;

public class UserRepositoryTest {
    private UserRepository userRepository;
    private User user;
    private final String userLogin = "test";

    @Before
    public void before() {
        userRepository = new UserRepository(new ConnectionService(new PropertyService()).getConnection());
        User user = new User();
        user.setLogin(userLogin);

        this.user = userRepository.add(user);
    }

    @Test
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());

        @NonNull final User projectById = userRepository.findById(user.getId());
        Assert.assertNotNull(projectById);
    }


    @Test
    public void findAll() {
        @NonNull final List<User> users = userRepository.findAll();
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findById() {
        Assert.assertNotNull(userRepository.findById(user.getId()));
    }

    @Test
    public void findByLogin() {
        Assert.assertNotNull(userRepository.findByLogin(user.getLogin()));
    }

    @Test
    public void removeById() {
        userRepository.removeById(user.getId());
        Assert.assertNull(userRepository.findById(user.getLogin()));
    }

    @Test
    public void removeByLogin() {
        userRepository.removeByLogin(user.getLogin());
        Assert.assertNull(userRepository.findByLogin(user.getLogin()));
    }
}
