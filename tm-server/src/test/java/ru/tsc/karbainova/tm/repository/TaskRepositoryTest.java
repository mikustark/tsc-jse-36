package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.service.ConnectionService;
import ru.tsc.karbainova.tm.service.PropertyService;

import java.util.List;

public class TaskRepositoryTest {

    @Nullable
    private TaskRepository taskRepository;
    @Nullable
    private Task task;
    private String userLogin = "test";

    @Before
    public void before() {
        taskRepository = new TaskRepository(new ConnectionService(new PropertyService()).getConnection());
        taskRepository.add(userLogin, new Task("Task"));
        task = taskRepository.findAll().get(0);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Task", task.getName());

        @NonNull final Task projectById = taskRepository.findById(task.getUserId(), task.getId());
        Assert.assertNotNull(projectById);
    }

    @Test
    public void findAll() {
        @NonNull final List<Task> projects = taskRepository.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserId() {
        @NonNull final List<Task> projects = taskRepository.findAll(userLogin);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByErrorUserId() {
        @NonNull final List<Task> projects = taskRepository.findAll("ertyut");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    public void findAllByName() {
        @NonNull final Task projects = taskRepository.findByName(userLogin, task.getName());
        Assert.assertNotNull(projects);
    }

    @Test
    public void findAllByErrorName() {
        @NonNull final Task projects = taskRepository.findByName(userLogin, "sdf");
        Assert.assertNull(projects);
    }

    @Test
    public void removeById() {
        taskRepository.remove(userLogin, task);
        Assert.assertNull(taskRepository.findById(userLogin, task.getId()));
    }

    @Test
    public void removeByErrorUserId() {
        taskRepository.removeById("sd", task.getId());
        @NonNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertEquals(0, tasks.size());
    }
}
